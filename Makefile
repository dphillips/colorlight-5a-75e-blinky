TOP = top
OBJS = top.v blink.v
SIM ?= icarus

FREQ_MHZ = 65
DEVICE_TYPE = 25k
PACKAGE = CABGA256
SPEED_GRADE = 6

.PHONY: all clean program lint sim
all: lint $(TOP).svf

clean:
	rm -f *.svf *.bit *.json

lint:
	iverilog -tnull $(OBJS)

sim:
	$(MAKE) -C sim

program: $(TOP).svf
	# probably not very useful to anyone without more configurable paths
	openocd -f /usr/share/openocd/scripts/interface/ftdi/dp_busblaster_kt-link.cfg \
	        -f ~/jtag/5a-75e.cfg \
	        -c "init; svf $^; shutdown"

%.json: $(OBJS)
	yosys -e ".*" -p "synth_ecp5 -json $@" $(OBJS)

%.config: %.json %.lpf
	nextpnr-ecp5 \
		--$(DEVICE_TYPE) \
		--package $(PACKAGE) \
		--speed $(SPEED_GRADE) \
		--freq $(FREQ_MHZ) \
		--json $< \
		--textcfg $@ \
		--lpf $(word 2,$^)

%.bit %.svf: %.config
	ecppack \
		--bit "$*.bit" \
		--svf "$*.svf" \
		--input "$^"
