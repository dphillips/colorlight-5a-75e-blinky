import cocotb
from cocotb.triggers import Timer, FallingEdge
from cocotb.clock import Clock


@cocotb.test()
def test_reset_assert_async(dut):
    """Test led_o is high-z when reset is asserted

    This should be asynchronous to clk_i.
    """
    dut.rst_i.value = 1

    yield Timer(1, 'ns')
    assert dut.led_o.value.binstr == 'z'

@cocotb.test()
async def test_reset_deassert_clocked(dut):
    """Test led_o is high and counter is zeroed after a reset deassertion is
    clocked in

    Unlike reset assertion, this should be synchronous to clk_i.
    """
    clock = Clock(dut.clk_i, 1, units="ns")
    cocotb.start_soon(clock.start())

    # reset the module across a clock cycle
    dut.rst_i.value = 1
    await FallingEdge(dut.clk_i)

    assert dut.led_o.value == 1
    assert dut.counter.value.integer == 0

@cocotb.test()
async def test_clock_toggle(dut):
    """Test led_o toggles every 8 clocks

    We set blink.OVERFLOW to 8 in this TB, so every 8 clocks, the value of
    led_o should be toggled.
    """
    clock = Clock(dut.clk_i, 2, units="ns")
    cocotb.start_soon(clock.start())

    # reset the module across a clock cycle
    dut.rst_i.value = 1
    await FallingEdge(dut.clk_i)
    dut.rst_i.value = 0

    for i in range(10):
        for i in range(8):
            assert dut.led_o.value == 1
            await FallingEdge(dut.clk_i)

        for i in range(8):
            assert dut.led_o.value == 0
            await FallingEdge(dut.clk_i)

