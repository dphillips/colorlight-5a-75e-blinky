module top(
	input clk_i,
	output led_o
);

blink #(
	.COUNTER_WIDTH(24),
	// 25 MHz clock => 1 Hz
	.OVERFLOW(12_500_000)
)
blink_inst(
	.clk_i(clk_i),
	.rst_i(1'b0),
	.led_o(led_o)
);

endmodule
