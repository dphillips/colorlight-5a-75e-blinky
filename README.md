![pipeline](https://gitlab.com/dphillips/colorlight-5a-75e-blinky/badges/main/pipeline.svg)

# Obligatory blinky for the 5A-75E

This quick-and-dirty design blinks the green user LED on the Colorlight 5A-75E
at 1 Hz with a 50% duty. It divides the 25 MHz crystal oscillator input to the
FPGA down with a counter to achieve this.

## CI/CD Pipeline

A pipeline runs to build the bitstream, and to run tests in a simulation of the
design. This is more the purpose of this project, acting as my own template for
CI/CD with Yosys+nextpnr.

## Notes

I've seen one of the HC245s decide to oscillate if you flash this bitstream
after letting the factory design start. It tried to draw my current limit of
500 mA, and would presumably draw more if allowed. It's probably due to this
design leaving unused I/Os in high-Z. It seems not to be able to get itself
into the state when I set a lower current limit and/or a lower voltage on the
board's main input, such as 300 mA and/or 3.3 V.

Another workaround is not to let the factory design load before loading blinky.

