module blink(
	input rst_i,
	input clk_i,
	output reg led_o
);

parameter COUNTER_WIDTH = 32;
parameter OVERFLOW = 25_000_000;

reg[COUNTER_WIDTH-1:0] counter;

always @(posedge clk_i) begin
	if (rst_i) begin
		counter <= 0;
		led_o <= 1;
	end else if (counter == OVERFLOW-1) begin
		led_o <= ~led_o;
		counter <= 0;
	end else
		counter <= counter + 1;
end

`ifdef COCOTB_SIM
initial begin
	$dumpfile("blink.vcd");
	$dumpvars(0, blink);
end
`endif

endmodule
